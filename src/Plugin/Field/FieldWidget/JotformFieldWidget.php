<?php

namespace Drupal\jotform_field_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\jotform_field_widget\JotformService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'jotform_field_widget' field widget.
 *
 * @FieldWidget(
 *   id = "jotform_field_widget",
 *   label = @Translation("Jotform Field Widget"),
 *   field_types = {"string"},
 * )
 */
final class JotformFieldWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * Service for Jotform.
   *
   * @var \Drupal\jotform_field_widget\JotformService
   */
  private JotformService $jotformService;

  /**
   * Module Handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private ModuleHandlerInterface $moduleHandler;

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    JotformService $jotform_field_widget_service,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings
    );

    $this->jotformService = $jotform_field_widget_service;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    return new self(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('jotform_field_widget.jotform'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element['value'] = $element + [
      // Use select2 if it's available.
      '#type' => ($this->moduleHandler->moduleExists('select2') ? 'select2' : 'select'),
      '#options' => [],
      '#default_value' => $items[$delta]->value ?? NULL,
    ];

    // Get the Jotform API client.
    $jotform_client = $this->jotformService->getClient();
    // Return an error if we were unable to get a client.
    if (!$jotform_client) {
      $this->messenger()->addError($this->t("There was a problem getting forms from Jotform. Please contact technical support."));
      return $element;
    }

    // Get forms from the Jotform API.
    $jotform_forms = $jotform_client->getForms(0, 1000);
    if (!$jotform_forms || count($jotform_forms) < 1) {
      $this->messenger()->addWarning($this->t("Jotform did not return any forms."));
      return $element;
    }
    else {
      foreach ($jotform_forms as $jotform_form) {
        // Get the form id, title and status.
        $id = $jotform_form['id'];
        $title = $jotform_form['title'];
        $status = $jotform_form['status'];
        // Display the ID and Status in the option label so that the user can
        // see whether the form they're using is enabled or not. ID may be
        // useful if there are two forms with the same name - who knows how well
        // maintained the forms may be?
        $element['value']['#options'][$id] = $this->t('@title (@id - @status)', [
          '@title' => $title,
          '@id' => $id,
          '@status' => $status,
        ]);
      }
    }

    return $element;
  }

}
