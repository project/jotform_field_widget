<?php

namespace Drupal\jotform_field_widget;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Service for jotform integration.
 */
class JotformService {

  use StringTranslationTrait;

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Logger object for jotform_field_widget channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The jotform client.
   *
   * @var \JotForm
   */
  protected $jotformClient;

  /**
   * Constructs the Service.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Drupal configuration object factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager service.
   */
  public function __construct(
    ConfigFactory $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->config = $config_factory->get('jotform_field_widget.settings');
    $this->logger = $logger_factory->get('jotform_field_widget');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get Jotform API Client.
   *
   * Instantiate a connection to the Jotform API and return the client.
   *
   * @returns \JotForm|FALSE
   *   The Jotform client or FALSE if unable to create.
   */
  public function getClient(): bool | \JotForm {
    // Load the api key.
    $api_key = $this->config->get('api_key');

    if (!$api_key) {
      $this->logger->error($this->t('Unable to create Jotform client, missing API key.'));
      return FALSE;
    }

    try {
      $this->jotformClient = new \JotForm($api_key);
      return $this->jotformClient;
    }
    catch (\Exception $exception) {
      $this->logger->error($exception->getMessage());
    }

    return FALSE;
  }

}
