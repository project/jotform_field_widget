## Introduction

Provides a field widget for selecting a Jotform Form Id, and a service for
instantiating a Jotform API client using
`https://github.com/jotform/jotform-api-php`.

## Configuration

`/admin/config/services/jotform-field-widget` provides a configuration form for
entering in a Jotform API key.
